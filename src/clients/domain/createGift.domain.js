const { CreateCardInputSchema } = require('../schemas/input/createCard.input');
const { createGiftServiceDB } = require('../service/createClient.service');

exports.createGiftDomain = async (eventPayload, eventMeta) => {
  new CreateCardInputSchema(eventPayload, eventMeta);
  const user = JSON.parse(eventPayload.Message);
  
  await createGiftServiceDB(user);

  return {
    body: {
      message: 'gift added successfully'
    }
  }
}