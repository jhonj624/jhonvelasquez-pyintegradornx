const { emitClientCreated } = require('../service/emitClientCreated.service');
const { CreateClientInputSchema } = require('../schemas/input/createClient.input');
const { getAge } = require('../helper/getAge.helper');
const { createClientServiceDB } = require('../service/createClient.service');
const { ClientCreatedSchema } = require('../schemas/event/clientCreated.event');

async function createClientDomain ( commandPayload, commandMeta) {
  new CreateClientInputSchema( commandPayload, commandMeta);

  const { dni, name, lastName, birthDate } = commandPayload;
  const age = getAge(birthDate);
  if ( age < 18 || age > 65 ) return {
    statusCode: 400,
    body: JSON.stringify(
      {
        message: 'You are not elegible for. Client must be between 18 and 65 years old',
        data: age
      })
  };
  
  const clientCreated = {
        dni,
        name,
        last_name: lastName,
        birth_date: birthDate
  }
  
  await createClientServiceDB(clientCreated);
  
  await emitClientCreated(new ClientCreatedSchema({...clientCreated, age}, commandMeta));

  return {
    statusCode: 200,
    body: {
      message: 'Input data is ok',
      data: clientCreated
    },
  };
};

module.exports = { createClientDomain };