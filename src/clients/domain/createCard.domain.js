const { CreateCardInputSchema } = require('../schemas/input/createCard.input.js');
const { createCardServiceDB } = require('../service/createClient.service');

exports.createCardDomain = async (eventPayload, eventMeta) => {
  new CreateCardInputSchema(eventPayload, eventMeta);
  const user = JSON.parse(eventPayload.Message);

  await createCardServiceDB(user);
  
  return {
    body: {
      message: 'card added successfully'
    }
  }
}
