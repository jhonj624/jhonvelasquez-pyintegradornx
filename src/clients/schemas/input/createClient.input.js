const { InputValidation } = require('ebased/schema/inputValidation');

class CreateClientInputSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.CREATE_CLIENT',
      payload: payload,
      source: meta.source,
      specversion: "v1.0.0",
      schema: {
        dni: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true},
        birthDate: { type: String, required: true},
      },
    });
  }
}
module.exports = { CreateClientInputSchema };