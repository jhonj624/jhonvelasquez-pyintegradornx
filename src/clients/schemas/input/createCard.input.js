const { InputValidation } = require('ebased/schema/inputValidation');

class CreateCardInputSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CARD.CREATE',
      payload: JSON.parse(payload.Message),
      meta: meta,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true },
        name: { type: String, required: true },
        last_name: { type: String, required: true},
        birth_date: { type: String, required: true},
        age: { type: Number, required: true}
      },
    })
  }
}

module.exports = { CreateCardInputSchema }