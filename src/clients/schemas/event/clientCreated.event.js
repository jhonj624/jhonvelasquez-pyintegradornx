const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientCreatedSchema extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.CLIENT_CREATED',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        dni: { type: String, required: true },
        name: { type: String, required: true },
        last_name: { type: String, required: true},
        birth_date: { type: String, required: true},
        age: { type: Number, required: true}
      },
    })
  }
}

module.exports = { ClientCreatedSchema };