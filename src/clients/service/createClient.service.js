const dynamo = require('ebased/service/storage/dynamo');
const { tableName } = require('../../../config/constants');
const { getGift } = require('../helper/getGift.helper');
const { getCard } = require('../helper/getCard.helper');

exports.createClientServiceDB = (commandPayload) => dynamo.putItem({
    TableName: tableName,
    Item: commandPayload
  });

exports.createGiftServiceDB = async user => {
  const gift = getGift();
  const params = {
    TableName: tableName,
    Key: {'dni': user.dni},
    UpdateExpression: 'set gift = :x',
    ExpressionAttributeValues:{
      ':x': gift
    }
  }
  await dynamo.updateItem(params);
}

exports.createCardServiceDB = async user => {
  const card = getCard(user.age);
  const params = {
    TableName: tableName,
    Key: {'dni': user.dni},
    UpdateExpression: 'set credit_card = :x',
    ExpressionAttributeValues:{
      ':x': card
    }
  }
  await dynamo.updateItem(params);
}
