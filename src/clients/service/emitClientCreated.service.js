const sns = require('ebased/service/downstream/sns');

// const CLIENT_CREATED_TOPIC = config.get('CLIENT_CREATED_TOPIC');
const { SNS_ARN_TOPIC } = require('../../../config/constants');

async function emitClientCreated(clientCreatedEvent) {
  const { eventPayload, eventMeta } = clientCreatedEvent.get();
  const snsPublishParams = {
    TopicArn: SNS_ARN_TOPIC,
    Message: eventPayload,
  };
  await sns.publish(snsPublishParams, eventMeta);

}

module.exports = { emitClientCreated };