const { AGE_CARD_DIVISORY } = require('../../../config/constants');

exports.getCard = age => {
  const typeCard = age < AGE_CARD_DIVISORY ? 'Clasic' : 'Gold';
  
  const card = {
    typeCard,
    accountNumber: '1234 5678 4568 3210',
    beforeTime: '10/27',
    ccv: '987'
  }
  return card;
}