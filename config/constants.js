exports.tableName = 'jhonVelasquez-clients';
exports.MINIMUN_AGE = 18;
exports.MAXIMUN_AGE = 65;
exports.AGE_CARD_DIVISORY = 45;
exports.SNS_ARN_TOPIC = 'arn:aws:sns:us-east-1:450865910417:jhonvelasquez-dev-clientCreated';
exports.CLIENTS_TABLE = 'arn:aws:dynamodb:us-east-1:450865910417:table/jhonVelasquez-clients'
